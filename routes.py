# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import shutil
import gzip
import os
import sys
import traceback
from shutil import copyfile
from functools import wraps
from tryton_replication import ReplManager
from trytond.transaction import Transaction
from trytond.cache import Cache
from trytond.config import config, split_netloc
from trytond.protocols.wrappers import with_pool, with_transaction
from tryton_rpc import Session
from trytond import backend
from trytond.wsgi import app
from cryptography.fernet import Fernet
from datetime import datetime, timedelta

# replica db path
OUT_PATH = config.get('database', 'path')
# replica template db path
TEMPLATE_PATH = config.get('replication', 'template_path')
# lifecycle to reuse existing replica db in path
DB_LIFE_CYCLE = config.getint('replication', 'db_life_cycle', default=30)


def generate_replica_db(func):
    @wraps(func)
    def wrapper(request, database_name, *args, **kwargs):
        user = request.args.get('user')

        kwargs = kwargs.copy() if kwargs else {}
        db_template_path = os.path.join(
            TEMPLATE_PATH, '%s.sqlite' % database_name)
        new_database_name = '%s-%s' % (user, database_name)
        db_out_path = os.path.join(OUT_PATH, '%s.sqlite' % new_database_name)

        kwargs['fresh_db'] = False
        if (not os.path.isfile(db_out_path)
                or datetime.fromtimestamp(os.path.getmtime(db_out_path))
                < (datetime.now() - timedelta(days=DB_LIFE_CYCLE))):
            copyfile(db_template_path, db_out_path)
            kwargs['fresh_db'] = True

        return func(request, new_database_name, *args, **kwargs)
    return wrapper


@app.route('/<database_name>/replication/download_database', methods=['GET'])
@generate_replica_db
@with_pool
@with_transaction(readonly=False)
def download_database(request, pool, **kwargs):
    user = request.args.get('user')
    pwd = request.args.get('pwd')
    fernet_key = config.get('replication', 'fernet_key')
    if not fernet_key:
        raise Exception('Missing Fernet key on replication section.')
    fernet = Fernet(fernet_key)
    pwd = fernet.decrypt(pwd.encode('utf-8')).decode()
    repl_dbname = request.args.get('remote_database')

    # login on remote-server
    hostname, port = split_netloc(config.get('replication', 'remote_host'))
    rpc_session = Session()
    rpc_session.login(user, pwd, hostname, port, repl_dbname)

    # replicate
    dbname = pool.database_name
    with Transaction().set_user(1):
        if kwargs.get('fresh_db'):
            try:
                ReplManager.init_db(rpc_session)
            except backend.DatabaseOperationalError:
                Transaction().rollback()
            except Exception:
                traceback.print_exc(file=sys.stdout)
                raise

        for x in range(0, 2):
            # replicate twice
            Cache.clear_all()
            try:
                ReplManager.replicate(rpc_session)
            except backend.DatabaseOperationalError:
                Transaction().rollback()
            except Exception:
                traceback.print_exc(file=sys.stdout)
                raise
            Transaction().commit()

    # compress
    db_path = os.path.join(OUT_PATH, '%s.sqlite' % dbname)
    gzip_db_path = '%s.gz' % db_path
    if os.path.isfile(gzip_db_path):
        os.remove(gzip_db_path)
    with open(db_path, 'rb') as f_in, gzip.open(gzip_db_path, 'wb') as f_out:
        shutil.copyfileobj(f_in, f_out)

    # return file content
    with open(gzip_db_path, 'rb') as f:
        return f.read()
