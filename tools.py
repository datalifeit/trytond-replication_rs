# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pyson import If, Eval

__all__ = ['skip_domain_on_synch']


def skip_domain_on_synch(domain):
    return [If(Eval('context', {}).get('synch', False),
        [], domain)]
