datalife_replication_rs
=======================

The replication_rs module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-replication_rs/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-replication_rs)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
