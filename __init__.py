# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
import tryton_replication
from . import routes
from .tools import skip_domain_on_synch


def register():
    Pool.register(
        tryton_replication.ReplDeleted,
        tryton_replication.Model,
        tryton_replication.Warning_,
        tryton_replication.UserLocalIdDest,
        module='replication_rs', type_='model')
